﻿using System.Collections.Generic;
using JobInvoiceGenerator.domain;
using JobInvoiceGenerator.ConsoleApp.model;

namespace JobInvoiceGenerator.ConsoleApp.translator
{
    public class PrintItemTranslator
    {
        public IList<PrintItem> translateToPrintItemDomainList(IList<PrintItemDto> requests)
        {
            var result = new List<PrintItem>();

            foreach (var request in requests)
            {
                result.Add(translateToPrintItemDomain(request));
            }

            return result;
        }

        public PrintItem translateToPrintItemDomain(PrintItemDto request)
        {
            var result = new PrintItem(request.name, request.price);

            if (request.isSalesTaxExempt)
            {
                result.SetSalesTaxStrategy(new ExemptSalesTaxStrategy());
            }

            return result;
        }
    }
}
