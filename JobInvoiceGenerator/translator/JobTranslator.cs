﻿using System.Collections.Generic;
using JobInvoiceGenerator.domain;
using JobInvoiceGenerator.ConsoleApp.model;

namespace JobInvoiceGenerator.ConsoleApp.translator
{
    public class JobTranslator
    {
        private readonly PrintItemTranslator printItemDtoTranslator;

        public JobTranslator(PrintItemTranslator printItemDtoTranslator)
        {
            this.printItemDtoTranslator = printItemDtoTranslator;
        }

        public IList<Job> translateToJobDomainList(IList<JobDto> jobRequests)
        {
            var result = new List<Job>();

            foreach (var JobRequestDto in jobRequests)
            {
                result.Add(translateToJobDomain(JobRequestDto));
            }

            return result;
        }

        public Job translateToJobDomain(JobDto jobRequest)
        {
            var printItems = printItemDtoTranslator.translateToPrintItemDomainList(jobRequest.printItems);

            Job result = new Job(jobRequest.jobName, printItems);

            if (jobRequest.hasExtraMargin)
            {
                result.SetMarginStrategy(new ExtraMarginStrategy());
            }

            return result;
        }

        public InvoiceResponseDto translateToInvoiceDto(IList<Job> jobs)
        {
            InvoiceResponseDto result = new InvoiceResponseDto();

            foreach (var job in jobs)
            {
                result.Add(translateToJobDto(job));
            }

            return result;
        }

        public JobSummaryResponseDto translateToJobDto(Job job)
        {
            var result = new JobSummaryResponseDto();

            result.JobName = job.GetName();

            foreach (var printItem in job.GetPrintItems())
            {
                result.printItems.Add(printItem.GetName(), string.Format("{0:C}", printItem.GetPriceWithTaxes()));
            }
            result.Total = string.Format("{0:C}", job.GetTotal());

            return result;
        }
    }
}
