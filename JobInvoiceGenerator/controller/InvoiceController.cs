﻿using System.Collections.Generic;
using JobInvoiceGenerator.ConsoleApp.model;
using JobInvoiceGenerator.ConsoleApp.translator;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.ConsoleApp.controller
{
    public class InvoiceController : IInvoiceController
    {
        private readonly JobTranslator jobTranslator;

        public InvoiceController(JobTranslator jobTranslator)
        {
            this.jobTranslator = jobTranslator;
        }

        public InvoiceResponseDto generateInvoice(IList<JobDto> request)
        {
            IList<Job> jobs = jobTranslator.translateToJobDomainList(request);
            InvoiceResponseDto invoice = jobTranslator.translateToInvoiceDto(jobs);
            return invoice;
        }
    }
}
