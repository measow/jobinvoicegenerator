﻿using System.Collections.Generic;
using JobInvoiceGenerator.ConsoleApp.model;

namespace JobInvoiceGenerator.ConsoleApp.controller
{
    public interface IInvoiceController
    {
        InvoiceResponseDto generateInvoice(IList<JobDto> request);
    }
}
