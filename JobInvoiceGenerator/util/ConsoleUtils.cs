﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using JobInvoiceGenerator.ConsoleApp.model;

namespace JobInvoiceGenerator.ConsoleApp
{
    public class ConsoleUtil
    {
        public static void validateInput(string[] args)
        {
            if (args.Length < 2)
            {
                throw new ArgumentException("First param must be the file path to the input file; " +
                    "second param must be the file path of the output file for which you'd like created.");
            }

            if (!File.Exists(args[0]))
            {
                throw new FileNotFoundException("The specified input file doesn't exist!");
            }

            string directoryName = Path.GetDirectoryName(args[1]);

            if (!Directory.Exists(directoryName))
            {
                throw new DirectoryNotFoundException("The specified output directory doesn't exist!");
            }
        }

        public static List<JobDto> parseInput(string inputFile)
        {
            List<JobDto> result;
            using (StreamReader streamReader = new StreamReader(inputFile))
            {
                string json = streamReader.ReadToEnd();
                result = JsonConvert.DeserializeObject<List<JobDto>>(json);
            }
            return result;
        }
    }
}
