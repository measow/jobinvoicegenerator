﻿using System;
using System.IO;
using System.Collections.Generic;
using StructureMap;
using JobInvoiceGenerator.ConsoleApp.controller;
using JobInvoiceGenerator.ConsoleApp.model;

namespace JobInvoiceGenerator.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleUtil.validateInput(args);
            IList<JobDto> request = ConsoleUtil.parseInput(args[0]);

            var container = Container.For<ConsoleRegistry>();
            var invoiceController = container.GetInstance<IInvoiceController>();

            InvoiceResponseDto response = invoiceController.generateInvoice(request);

            string outputFile = args[1];
            File.WriteAllText(outputFile, response.ToString());

            Console.WriteLine("Output file has been generated and can be found here: " + outputFile);
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}

