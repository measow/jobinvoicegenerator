﻿using System.Collections.Generic;
using System.Text;

namespace JobInvoiceGenerator.ConsoleApp.model
{
    public class JobSummaryResponseDto
    {
        public string JobName { get; set; }
        public IDictionary<string, string> printItems = new Dictionary<string, string>();
        public string Total { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}", JobName);
            sb.AppendLine();
            foreach (KeyValuePair<string, string> printItem in printItems)
            {
                sb.AppendFormat("{0}: {1}", printItem.Key, printItem.Value);
                sb.AppendLine();
            }
            sb.AppendFormat("Total: {0}", Total);
            sb.AppendLine();

            return sb.ToString();
        }

    }
}
