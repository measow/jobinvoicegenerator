﻿using System.Collections.Generic;
using System.Text;

namespace JobInvoiceGenerator.ConsoleApp.model
{
    public class InvoiceResponseDto
    {
        public IList<JobSummaryResponseDto> jobSummaryList = new List<JobSummaryResponseDto>();

        public void Add(JobSummaryResponseDto jobSummary)
        {
            jobSummaryList.Add(jobSummary);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("********** Job Invoice ***************");
            sb.AppendLine();
            foreach (var jobSummary in jobSummaryList)
            {
                sb.AppendFormat(jobSummary.ToString());
            }
            sb.AppendFormat("***************************************");
            return sb.ToString();
        }
    }
}
