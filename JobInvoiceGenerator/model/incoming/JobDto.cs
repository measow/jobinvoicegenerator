﻿using System.Collections.Generic;

namespace JobInvoiceGenerator.ConsoleApp.model
{
    public class JobDto
    {
        public string jobName;
        public bool hasExtraMargin;
        public List<PrintItemDto> printItems;
    }
}
