﻿namespace JobInvoiceGenerator.ConsoleApp.model
{
    public class PrintItemDto
    {
        public string name;
        public decimal price;
        public bool isSalesTaxExempt;
    }
}
