﻿using NUnit.Framework;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.Test
{
    [TestFixture]
    public class PrintItemTests
    {
        // TODO: Convert to parameterized test
        [Test]
        public void shouldRoundToNearestCent()
        {
            // Arrange
            var printItem = new PrintItem("Any Name", 100.05m);

            // Act
            decimal priceWithTaxes = printItem.GetPriceWithTaxes();

            // Assert
            Assert.AreEqual(107.05m, priceWithTaxes); // 100.05 + (100.05 * .07) = 107.0535 -> 107.05
        }
    }
}
