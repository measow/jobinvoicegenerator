﻿using NUnit.Framework;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.Test
{
    [TestFixture]
    public class StandardSalesTaxStrategyTests
    {
        // TODO: Convert to parameterized test
        [Test]
        public void shouldReturnSevenPercentOfPrice()
        {
            // Arrange
            var taxStrategy = new StandardSalesTaxStrategy();

            // Act
            decimal tax = taxStrategy.calculateSalesTax(new PrintItem("Any Name", 100.00m));

            // Assert
            Assert.AreEqual(7.00m, tax);
        }
    }
}
