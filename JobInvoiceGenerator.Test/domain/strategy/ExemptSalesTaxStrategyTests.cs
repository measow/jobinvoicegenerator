﻿using NUnit.Framework;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.Test
{
    [TestFixture]
    public class ExemptSalesTaxStrategyTests
    {
        // TODO: Convert to parameterized test
        [Test]
        public void shouldReturnZero()
        {
            // Arrange
            var taxStrategy = new ExemptSalesTaxStrategy();

            // Act
            decimal tax = taxStrategy.calculateSalesTax(new PrintItem("Any Name", 100.00m));

            // Assert
            Assert.AreEqual(0m, tax);
        }
    }
}
