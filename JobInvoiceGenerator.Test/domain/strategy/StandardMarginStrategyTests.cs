﻿using System.Collections.Generic;
using NUnit.Framework;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.Test
{
    [TestFixture]
    public class StandardMarginStrategyTests
    {
        // TODO: Convert to parameterized test
        [Test]
        public void shouldReturnElevenPercentOfPrice()
        {
            // Arrange
            var marginStrategy = new StandardMarginStrategy();

            // Act
            decimal margin = marginStrategy.calculateMargin(new List<PrintItem>() { new PrintItem("Any Name", 100.00m) });

            // Assert
            Assert.AreEqual(11.00m, margin);
        }
    }
}
