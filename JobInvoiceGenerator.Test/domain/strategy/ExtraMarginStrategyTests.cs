﻿using System.Collections.Generic;
using NUnit.Framework;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.Test
{
    [TestFixture]
    public class ExtraMarginStrategyTests
    {
        // TODO: Convert to parameterized test
        [Test]
        public void shouldReturnSixteenPercentOfPrice()
        {
            // Arrange
            var marginStrategy = new ExtraMarginStrategy();

            // Act
            decimal margin = marginStrategy.calculateMargin(new List<PrintItem>() { new PrintItem("Any Name", 100.00m) });

            // Assert
            Assert.AreEqual(16.00m, margin);
        }
    }
}
