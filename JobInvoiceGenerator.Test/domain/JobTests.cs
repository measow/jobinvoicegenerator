﻿using System.Collections.Generic;
using NUnit.Framework;
using JobInvoiceGenerator.domain;

namespace JobInvoiceGenerator.Test
{
    [TestFixture]
    public class JobTests
    {
        // TODO: Convert to parameterized test
        [Test]
        public void shouldRoundToNearestEvenCent()
        {
            // Arrange
            var job = new Job("Any Name", new List<PrintItem>() {
                new PrintItem("Any Name", 100.04m)
            });

            // Act
            decimal total = job.GetTotal();

            // Assert
            Assert.AreEqual(118.04m, total); // 100.04 + 7.0028 + 11.0044 = 118.0472 -> 118.04
        }
    }
}
