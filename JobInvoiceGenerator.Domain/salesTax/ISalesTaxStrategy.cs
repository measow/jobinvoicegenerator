﻿namespace JobInvoiceGenerator.domain
{
    public interface ISalesTaxStrategy
    {
        decimal calculateSalesTax( PrintItem jobItem );
    }
}
