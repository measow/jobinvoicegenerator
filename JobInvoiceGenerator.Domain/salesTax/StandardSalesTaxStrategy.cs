﻿namespace JobInvoiceGenerator.domain
{
    public class StandardSalesTaxStrategy : ISalesTaxStrategy
    {
        private const decimal TAX_RATE = .07m;

        public decimal calculateSalesTax(PrintItem jobItem)
        {
            return jobItem.GetPrice() * TAX_RATE;
        }
    }
}
