﻿using System;

namespace JobInvoiceGenerator.domain
{
    public class ExemptSalesTaxStrategy : ISalesTaxStrategy
    {
        public decimal calculateSalesTax(PrintItem jobItem)
        {
            return 0;
        }
    }
}
