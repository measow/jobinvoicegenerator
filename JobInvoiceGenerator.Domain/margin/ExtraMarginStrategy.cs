﻿using System.Collections.Generic;

namespace JobInvoiceGenerator.domain
{
    public class ExtraMarginStrategy : IMarginStrategy
    {
        private readonly IMarginStrategy marginStrategy;

        private const decimal ADDED_RATE = .05m;

        public ExtraMarginStrategy()
        {
            marginStrategy = new StandardMarginStrategy();
        }

        public decimal calculateMargin(IList<PrintItem> printItems)
        {
            decimal result = marginStrategy.calculateMargin(printItems);

            foreach (PrintItem item in printItems)
            {
                result += (item.GetPrice() * ADDED_RATE);
            }

            return result;
        }
    }
}
