﻿using System;
using System.Collections.Generic;

namespace JobInvoiceGenerator.domain
{
    public class StandardMarginStrategy : IMarginStrategy
    {
        private const decimal STANDARD_RATE = .11m;

        public decimal calculateMargin(IList<PrintItem> printItems)
        {
            decimal result = 0;

            foreach (PrintItem item in printItems)
            {
                result += (item.GetPrice() * STANDARD_RATE);
            }

            return result;
        }
    }
}
