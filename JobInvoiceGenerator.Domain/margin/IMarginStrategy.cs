﻿using System.Collections.Generic;

namespace JobInvoiceGenerator.domain
{
    public interface IMarginStrategy
    {
        decimal calculateMargin(IList<PrintItem> printItems);
    }
}
