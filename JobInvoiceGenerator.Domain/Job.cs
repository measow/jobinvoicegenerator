﻿using System.Collections.Generic;

namespace JobInvoiceGenerator.domain
{
    public class Job
    {
        private readonly string name;
        private readonly IList<PrintItem> printItems;
        private IMarginStrategy marginStrategy = new StandardMarginStrategy();

        public Job(string name, IList<PrintItem> printItems)
        {
            this.name = name;
            this.printItems = printItems;
        }

        public string GetName()
        {
            return name;
        }

        public IList<PrintItem> GetPrintItems()
        {
            return printItems;
        }

        public decimal GetTotal()
        {
            decimal value = 0;

            foreach (PrintItem printItem in printItems)
            {
                value += printItem.GetPriceWithTaxes();
            }

            value += marginStrategy.calculateMargin(printItems);

            return (0.02m / 1.00m) * decimal.Round(value * (1.00m / 0.02m));
        }

        public void SetMarginStrategy(IMarginStrategy marginStrategy)
        {
            this.marginStrategy = marginStrategy;
        }
    }
}
