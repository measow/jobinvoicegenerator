﻿using System;

namespace JobInvoiceGenerator.domain
{
    public class PrintItem
    {
        private readonly string name;
        private readonly decimal price;
        private ISalesTaxStrategy salesTaxStrategy = new StandardSalesTaxStrategy();

        public PrintItem(string name, decimal price)
        {
            this.name = name;
            this.price = price;
        }

        public string GetName()
        {
            return name;
        }

        public decimal GetPrice()
        {
            return price;
        }

        public decimal GetPriceWithTaxes()
        {
            decimal result = GetPrice() + salesTaxStrategy.calculateSalesTax(this);
            return Math.Round(result, 2, MidpointRounding.AwayFromZero);
        }

        public void SetSalesTaxStrategy(ISalesTaxStrategy salesTaxStrategy)
        {
            this.salesTaxStrategy = salesTaxStrategy;
        }
    }
}
